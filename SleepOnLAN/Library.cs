﻿using System;
//using System.IO;
using System.Security.Principal;
using System.Linq;
using System.Configuration;

namespace SleepOnLAN
{
    public static class Library
    {
        public static void WriteLog(ref System.Diagnostics.EventLog eventLog, Exception ex)
        {
            WriteLog(ref eventLog, ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim(), System.Diagnostics.EventLogEntryType.Error);
        }

        public static void WriteLog(ref System.Diagnostics.EventLog eventLog, string Message, System.Diagnostics.EventLogEntryType type = System.Diagnostics.EventLogEntryType.Information)
        {
            //StreamWriter sw = null;
            string now = DateTime.Now.ToString();
            //try
            //{
            //    sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\log.txt", true);
            //    sw.WriteLine(now + ": " + Message);
            //    sw.Flush();
            //    sw.Close();
            //}
            //catch
            //{
            //}

            Console.WriteLine(now + ": " + Message);

            eventLog.WriteEntry(now + ": " + Message, type);
        }

        public static bool IsUserAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin;
            WindowsIdentity user = null;
            try
            {
                //get the currently logged in user
                user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                isAdmin = false;
            }
            catch (Exception ex)
            {
                isAdmin = false;
            }
            finally
            {
                if (user != null)
                    user.Dispose();
            }
            return isAdmin;
        }

        public static bool ConfigContainsKey(KeyValueConfigurationCollection collection, string key)
        {
            if (collection[key] == null)
            {
                return collection.AllKeys.Contains(key);
            }

            return true;
        }


    }
}
