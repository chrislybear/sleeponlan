﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Linq;
using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;
using System.Diagnostics;
using System.Threading;

namespace SleepOnLAN
{
    public partial class Pcap : ServiceBase
    {
        private PacketCommunicator communicator = null;
        private static Configuration configManager = null;
        private static KeyValueConfigurationCollection confCollection = null;


        public Pcap()
        {
            InitializeComponent();
        }

        internal void WriteLog(string message)
        {
            Library.WriteLog(ref eventLog1, message);
        }

        internal void ConfigureService()
        {
            configManager = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            confCollection = configManager.AppSettings.Settings;


            // Retrieve the device list from the local machine
            IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;
            if (allDevices.Count == 0)
            {
                Library.WriteLog(ref eventLog1, "No interfaces found! Make sure WinPcap is installed.");
                return;
            }
            int deviceIndex = GetInterfaceSelection(allDevices);

            SetConfig("Interface", deviceIndex.ToString());

            // Regex for MAC addresses
            string rgex_mac = @"^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";

            // Target MAC address
            string mac;
            ReadInputWithRegexCheck(rgex_mac,
                                    "Enter the MAC address of the SOL destination (ff:ff:ff:ff:ff:ff or ff-ff-ff-ff-ff-ff format):",
                                    out mac);

            SetConfig("MACAdress", mac);

            // Password
            string pw;
            ReadInputWithRegexCheck(rgex_mac,
                                    "Enter the password for the SOL destination(ff: ff:ff: ff:ff: ff or ff-ff-ff-ff-ff-ff format):",
                                    out pw);

            SetConfig("Password", pw);

            // Save
            configManager.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configManager.AppSettings.SectionInformation.Name);
        }

        private static int GetInterfaceSelection(IList<LivePacketDevice> allDevices)
        {
            // Print the list
            for (int i = 0; i != allDevices.Count; ++i)
            {
                LivePacketDevice device = allDevices[i];
                Console.Write((i + 1) + ". " + device.Name);
                if (device.Description != null)
                    Console.WriteLine(" (" + device.Description + ")");
                else
                    Console.WriteLine(" (No description available)");
            }

            int deviceIndex = 0;
            do
            {
                Console.WriteLine("Enter the interface number (1-" + allDevices.Count + "):");
                string deviceIndexString = Console.ReadLine();
                if (!int.TryParse(deviceIndexString, out deviceIndex) ||
                    deviceIndex < 1 || deviceIndex > allDevices.Count)
                {
                    deviceIndex = 0;
                }
            } while (deviceIndex == 0);
            return deviceIndex;
        }

        private static void ReadInputWithRegexCheck(string rgex, string message, out string result)
        {
            Regex rgx = new Regex(rgex);
            result = "";
            do
            {
                Console.WriteLine(message);
                result = Console.ReadLine();
            } while (!rgx.IsMatch(result));
        }

        private static void SetConfig(string key, string value)
        {
            if (!Library.ConfigContainsKey(confCollection, key))
            {
                confCollection.Add(new KeyValueConfigurationElement(key, value));
            }
            else
            {
                confCollection[key].Value = value;
            }
        }

        internal void CheckConfig()
        {
            configManager = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            confCollection = configManager.AppSettings.Settings;

            Regex rgx = new Regex(@"^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$");
            Regex rgxif = new Regex(@"^[1-9][0-9]*$");

            if (!Library.ConfigContainsKey(confCollection, "Password") ||
                !Library.ConfigContainsKey(confCollection, "MACAdress") ||
                !Library.ConfigContainsKey(confCollection, "Interface") ||
                !rgx.IsMatch(confCollection["Password"].Value) ||
                !rgx.IsMatch(confCollection["MACAdress"].Value) ||
                !rgxif.IsMatch(confCollection["Interface"].Value)
                )
            {
                ExitConfigError("Configuration faulty. Run this program with --configure to generate a valid configuration.");
            }
            

        }

        private void ExitConfigError(string Msg)
        {
            Library.WriteLog(ref eventLog1, Msg);
            ExitCode = 15010;
            Stop();
            Environment.FailFast(Msg);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CheckConfig();
            }
            catch
            {
                ExitConfigError("Configruation problem!");
            }
            var worker = new Thread(RunCapture);
            worker.Name = "CaptureThread";
            worker.IsBackground = false;
            worker.Start();
            
        }

        protected override void OnStop()
        {
            communicator.Break();
            Library.WriteLog(ref eventLog1, "SleepOnLAN service stopped");
        }

        internal void RunCapture()
        {
            configManager = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            confCollection = configManager.AppSettings.Settings;
            


            // Retrieve the device list from the local machine
            IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;
            Int32.TryParse(confCollection["Interface"].Value, out int deviceIndex);

            if (deviceIndex == 0)
            {
                ExitConfigError("Configruation problem! Interface value missing or wrong.");
            }

            PacketDevice selectedDevice = allDevices[deviceIndex - 1];
            // Open the device
            using (communicator =
                selectedDevice.Open(65536,                                  // portion of the packet to capture
                                                                            // 65536 guarantees that the whole packet will be captured on all the link layers
                                    PacketDeviceOpenAttributes.Promiscuous, // promiscuous mode
                                    1000))                                  // read timeout
            {
                // Check the link layer. We support only Ethernet for simplicity.
                if (communicator.DataLink.Kind != DataLinkKind.Ethernet)
                {
                    Library.WriteLog(ref eventLog1, "This program works only on Ethernet networks.");
                    Environment.FailFast("This program works only on Ethernet networks.");
                    return;
                }

                // Compile the filter
                using (BerkeleyPacketFilter filter = this.communicator.CreateFilter("ip and udp port 9"))
                {
                    // Set the filter
                    communicator.SetFilter(filter);
                }

                Library.WriteLog(ref eventLog1, "Listening on " + selectedDevice.Description + "...");

                // start the capture
                Packet packet;
                do
                {
                    PacketCommunicatorReceiveResult result = communicator.ReceivePacket(out packet);

                    switch (result)
                    {
                        case PacketCommunicatorReceiveResult.Timeout:
                            // Timeout elapsed
                            continue;
                        case PacketCommunicatorReceiveResult.Ok:
                            PacketHandler(packet);
                            break;
                        default:
                            throw new InvalidOperationException("The result " + result + " shoudl never be reached here");
                    }

                } while (true);

            }


        }

        private static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        private void PacketHandler(Packet packet)
        {
            // print timestamp and length of the packet
            Library.WriteLog(ref eventLog1, packet.Timestamp.ToString("yyyy-MM-dd hh:mm:ss.fff") + " length:" + packet.Length);

            if (packet.Ethernet.Destination.Equals(new PcapDotNet.Packets.Ethernet.MacAddress("ff:ff:ff:ff:ff:ff")))
            {
                //byte[] payload = packet.IpV4.Udp.Payload.Bu
            }

            UdpDatagram udp = null;
            TcpDatagram tcp = null;
            Datagram datagram = null;
            byte[] payload = null;

            IpV4Datagram ip4 = packet.Ethernet.IpV4;
            if (ip4.Protocol == IpV4Protocol.Udp)
            {
                udp = ip4.Udp;
                datagram = udp.Payload;
            }
            if (ip4.Protocol == IpV4Protocol.Tcp)
            {
                tcp = ip4.Tcp;
                datagram = tcp.Payload;
            }
            if (null != datagram)
            {
                int payloadLength = datagram.Length;
                using (MemoryStream ms = datagram.ToMemoryStream())
                {
                    payload = new byte[payloadLength];
                    ms.Read(payload, 0, payloadLength);
                }
            }


            bool isBroadcast = (BitConverter.ToString(payload.Take(6).ToArray()).Replace("-", "") == new String('F', 12));
            bool pwCorrect = (confCollection["Password"].Value.Replace(":", "").Replace("-", "").ToUpper() ==
                                                BitConverter.ToString(payload.Reverse().Take(6).Reverse().ToArray()).Replace("-", ""));

            byte[] magic = payload.Skip(6).Reverse().Skip(6).Reverse().ToArray();
            byte[] bMac = StringToByteArray(confCollection["MACAdress"].Value.Replace(":", "").Replace("-", "").ToUpper());
            int size = 6;
            byte[][] bMacs = new byte[magic.Length / size][];
            for (var i = 0; i < magic.Length / size; i++)
            {
                bMacs[i] = magic.Skip(i * size).Take(size).ToArray();
            }

            bool macCorrect = true;
            foreach (byte[] m in bMacs)
            {
                macCorrect &= bMac.SequenceEqual(m);
            }


            // print ip addresses and udp ports
            Library.WriteLog(ref eventLog1, ip4.Source + ":" + udp.SourcePort + " -> " + ip4.Destination + ":" + udp.DestinationPort);
            if (isBroadcast && pwCorrect && macCorrect)
            {
                ShutdownMachine();
            }
        }

        private void ShutdownMachine()
        {
            Library.WriteLog(ref eventLog1, "Shutdown packet received. Initiating shutdown.");
            var psi = new ProcessStartInfo("shutdown", "/s /t 60");
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            Process.Start(psi);
        }
    }
}
