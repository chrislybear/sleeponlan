﻿using System;
using System.Configuration.Install;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;


namespace SleepOnLAN
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (System.Environment.UserInteractive)
            {

                if(args.Length == 0)
                {
                    Pcap p = new Pcap();
                    p.WriteLog("Starting capture process manually...");
                    p.CheckConfig();
                    p.RunCapture();
                    return;
                }

                string parameter = string.Concat(args);
                //parameter = "--configure";
                try
                {
                    switch (parameter)
                    {
                        case "--install":
                            if (!Library.IsUserAdministrator())
                            {
                                RunAsAdminWithArguments(new string[] { "--install"});
                            }
                            else
                            {
                                InstallService();
                            }
                            break;
                        case "--uninstall":
                            if (!Library.IsUserAdministrator())
                            {
                                RunAsAdminWithArguments(new string[] { "--uninstall" });
                            }
                            else
                            {
                                UninstallService();
                            }
                            break;
                        case "--configure":
                            configure();
                            break;
                        default:
                            Console.WriteLine("Use either --install, --uninstall or --configure as arguments");
                            break;
                    }
                }
                catch (System.Security.SecurityException scx)
                {
                    Console.WriteLine(scx.Message + " " + scx.FirstPermissionThatFailed.ToString());
                    Console.ReadKey();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("\r\n\r\nError: " + ex.Message.ToString());
                    Console.WriteLine("Press any key to exit...");
                    Console.ReadKey();
                }
            }
            else
            {

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new Pcap()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        private static void RunAsAdminWithArguments(string[] args)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = Assembly.GetExecutingAssembly().Location;
            psi.Arguments = string.Concat(args);
            psi.Verb = "runas";
            //psi.UseShellExecute = false;

            var process = new Process();
            process.StartInfo = psi;
            process.Start();
            process.WaitForExit();
        }

        //[PrincipalPermission(SecurityAction.Demand, Role = "BUILTIN\\Administrators")]
        private static void UninstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
        }

        //[PrincipalPermission(SecurityAction.Demand, Role = "BUILTIN\\Administrators")]
        private static void InstallService()
        {
            ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
            Pcap p = new Pcap();
            // Create the source, if it does not already exist.
            if (!EventLog.SourceExists(p.EventLog.Source))
            {
                EventLog.CreateEventSource(p.EventLog.Source, p.EventLog.Log);
                Console.WriteLine("Creating EventSource");
            }
            else
            {
                Console.WriteLine("EventSource exists");
            }
            Console.ReadKey();
        }

        private static void configure()
        {
            Pcap p = new Pcap();
            p.ConfigureService();
        }
    }
}
